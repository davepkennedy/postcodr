# Postcodr

An application which looks up address details for postcodes

## Building

Check out the sourcecode, run ./gradlew test assemble.

## Use

Run
`java -Dapikey=ak_iy95dv7gU8317fLD2gtc8D6PN9cUW -jar build/libs/postcodr-0.0.1-SNAPSHOT.jar`

Data is obtained from Ideal Postcodes which charges 2p per lookup.
The apikey provided will eventually run out of lookups…

