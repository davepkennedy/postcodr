package io.github.davepkennedy;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import uk.co.idealpostcodes.WebAddressProvider;

@Configuration
public class TestConfiguration {
    @Bean
    public AddressProvider addressProvider () {
        return new WebAddressProvider();
    }

    @Bean
    public WebClient clientProvider () {
        return new WebClientImpl();
    }
}
