package uk.co.idealpostcodes;

import io.github.davepkennedy.Address;
import io.github.davepkennedy.AddressProvider;
import io.github.davepkennedy.TestConfiguration;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/*
Unlike the tests in WebAddressProviderTest, these ones do actually communicate
with the server at Ideal Postcodes.

Normally requests will count in an account, and the provided API key here
will likely have no requests remaining on it, but it is a valid key.

These four postcodes will trigger the various responses on the server
and we can verify that we communicate with their system correctly and can
handle the expected success and failure states.
*/

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {TestConfiguration.class})
public class WebAddressProviderIntTest {
    private static final String API_KEY = "ak_iy6f32ap8wzEKac9CuySGPgZ6SLut";

    private static final String VALID_POSTCODE                  = "ID1 1QD";
    private static final String NON_EXISTENT_POSTCODE           = "ID1 KFA";
    private static final String NO_LOOKUPS_REMAINING_POSTCODE   = "ID1 CLIP";
    private static final String LIMIT_REACHED_POSTCODE          = "ID1 CHOP";

    private static final String TEST_TOWN = "Hammersmith and Fulham";

    @Autowired
    private AddressProvider addressProvider;

    @Before
    public void setup () {
        System.setProperty("apikey", API_KEY);
    }

    @Test
    public void validPostcodeRetrievesAddresses () {
        AtomicInteger errorCount = new AtomicInteger();
        List<Address> addresses = new ArrayList<>();
        addressProvider.fetchForPostcodes(VALID_POSTCODE).subscribe(
                addresses::add,
                error -> {errorCount.incrementAndGet();}
        );
        assertEquals(0, errorCount.get());
        assertNotEquals(0, addresses.size());
        assertEquals(VALID_POSTCODE, addresses.get(0).postcode());
        assertEquals(TEST_TOWN, addresses.get(0).town());
    }

    @Test
    public void invalidPostcodeIsNotFound () {
        AtomicInteger errorCount = new AtomicInteger();
        List<Address> addresses = new ArrayList<>();
        addressProvider.fetchForPostcodes(NON_EXISTENT_POSTCODE).subscribe(
                addresses::add,
                error -> {errorCount.incrementAndGet();}
        );
        assertEquals("There should be no addresses from this request", 0, addresses.size());
        assertEquals("There should be 1 error in this request", 1, errorCount.get());
    }

    @Test
    public void noLookupsAvailable () {
        AtomicInteger errorCount = new AtomicInteger();
        List<Address> addresses = new ArrayList<>();
        addressProvider.fetchForPostcodes(NO_LOOKUPS_REMAINING_POSTCODE).subscribe(
                addresses::add,
                error -> {errorCount.incrementAndGet();}
        );
        assertEquals("There should be no addresses from this request", 0, addresses.size());
        assertEquals("There should be 1 error in this request", 1, errorCount.get());
    }

    @Test
    public void requestLimitReached () {
        AtomicInteger errorCount = new AtomicInteger();
        List<Address> addresses = new ArrayList<>();
        addressProvider.fetchForPostcodes(LIMIT_REACHED_POSTCODE).subscribe(
                addresses::add,
                error -> {errorCount.incrementAndGet();}
        );
        assertEquals("There should be no addresses from this request", 0, addresses.size());
        assertEquals("There should be 1 error in this request", 1, errorCount.get());
    }
}
