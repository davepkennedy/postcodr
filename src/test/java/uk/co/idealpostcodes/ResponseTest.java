package uk.co.idealpostcodes;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

/*
This test is going to affect test coverage metrics.
Once we're sure we've got the Response/Result objects correctely annotated,
then this class should be deleted.

Any coverage of the Response class should be from other tests.
 */
public class ResponseTest {

    private static final String ADRESS_DATA =
            "{\"result\":[{\"postcode\":\"SW1A 2AA\",\"postcode_inward\":\"2AA" +
                    "\",\"postcode_outward\":\"SW1A\",\"post_town\":\"LONDON\"" +
                    ",\"dependant_locality\":\"\",\"double_dependant_locality\"" +
                    ":\"\",\"thoroughfare\":\"Downing Street\",\"dependant_tho" +
                    "roughfare\":\"\",\"building_number\":\"10\",\"building_na" +
                    "me\":\"\",\"sub_building_name\":\"\",\"po_box\":\"\",\"de" +
                    "partment_name\":\"\",\"organisation_name\":\"Prime Minist" +
                    "er & First Lord Of The Treasury\",\"udprn\":23747771,\"um" +
                    "prn\":\"\",\"postcode_type\":\"L\",\"su_organisation_indi" +
                    "cator\":\"\",\"delivery_point_suffix\":\"1A\",\"line_1\":" +
                    "\"Prime Minister & First Lord Of The Treasury\",\"line_2\"" +
                    ":\"10 Downing Street\",\"line_3\":\"\",\"premise\":\"10\"" +
                    ",\"longitude\":-0.127695242183412,\"latitude\":51.5035398" +
                    "826274,\"eastings\":530047,\"northings\":179951,\"country" +
                    "\":\"England\",\"traditional_county\":\"Greater London\"," +
                    "\"administrative_county\":\"\",\"postal_county\":\"London" +
                    "\",\"county\":\"London\",\"district\":\"Westminster\",\"w" +
                    "ard\":\"St James's\"}],\"code\":2000,\"message\":\"Succes" +
                    "s\"}";

    /*
    We probably won't use all these fields in the end. Once this file is
    removed, then fields we don't use should be uncovered in coverage reports
    and deleted.
    */
    private static final String POSTCODE            = "SW1A 2AA";
    private static final String ADDR_LINE_1         = "Prime Minister & First Lord Of The Treasury";
    private static final String ADDR_LINE_2         = "10 Downing Street";
    private static final String THOROUGHFARE        = "Downing Street";
    private static final String COUNTY              = "London";
    private static final String COUNTRY             = "England";
    private static final String ORGANISATION_NAME   = "Prime Minister & First Lord Of The Treasury";
    private static final String DISTRICT            = "Westminster";

    private Response response;

    @Before
    public void setup () throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();

        response = objectMapper.readValue (ADRESS_DATA, Response.class);
    }

    @Test
    public void thereIsOneAddress () {
        assertEquals (1, response.results().size());
    }

    @Test
    public void postcodeIsExtracted () {
        Result address = response.results().get(0);
        assertEquals(POSTCODE, address.postcode());
    }

    @Test
    public void addressLinesAreExtracted () {
        Result address = response.results().get(0);
        assertEquals(ADDR_LINE_1, address.line1());
        assertEquals(ADDR_LINE_2, address.line2());
    }

    @Test
    public void thoroughfareIsExtracted () {
        Result address = response.results().get(0);
        assertEquals(THOROUGHFARE, address.thoroughfare());
    }

    @Test
    public void countyIsExtracted () {
        Result address = response.results().get(0);
        assertEquals(COUNTY, address.county());
    }

    @Test
    public void countryIsExtracted () {
        Result address = response.results().get(0);
        assertEquals(COUNTRY, address.country());
    }

    @Test
    public void organisationIsExtracted () {
        Result address = response.results().get(0);
        assertEquals(ORGANISATION_NAME, address.organisation());
    }

    @Test
    public void districtIsExtracted () {
        Result address = response.results().get(0);
        assertEquals(DISTRICT, address.district());
    }
}
