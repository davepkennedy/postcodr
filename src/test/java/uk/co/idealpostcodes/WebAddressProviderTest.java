package uk.co.idealpostcodes;

import io.github.davepkennedy.AddressProvider;
import io.github.davepkennedy.TestConfiguration;
import io.github.davepkennedy.WebClient;
import io.reactivex.Observable;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;

public class WebAddressProviderTest {
    /*
    Four main responses documented by Ideal Postcode
    - OK
    - Not Found
    - Out of lookups
    - Request limit reasched
    */
    @RunWith(SpringRunner.class)
    @SpringBootTest
    @ContextConfiguration(classes = {TestConfiguration.class})
    public static class WhenAPostcodeIsFound {

        private static final String TEST_DISTRICT = "DISTRICT";
        private static final String TEST_POSTCODE = "POSTCODE";
        private static final String TEST_THOROUGHFARE = "THOROUGHFARE";
        private static final String TEST_LINE_1 = "LINE1";
        private static final String TEST_LINE_2 = "LINE2";
        private static final String TEST_ADDRESS = String.join(",", TEST_LINE_1, TEST_LINE_2);

        @MockBean
        private WebClient webClient;

        @Autowired
        private AddressProvider addressProvider;

        @MockBean
        private Result addressResult;
        @MockBean
        private Response response;


        @Before
        public void setup () {

            given (addressResult.line1()).willReturn(TEST_LINE_1);
            given (addressResult.line2()).willReturn(TEST_LINE_2);
            given (addressResult.postcode()).willReturn(TEST_POSTCODE);
            given (addressResult.district()).willReturn(TEST_DISTRICT);

            given (response.results()).willReturn(Arrays.asList(addressResult));

            given (webClient.fetchUrl(anyString(), any()))
                    .willReturn(Observable.just(response));
        }

        @Test
        public void thenThereShouldBeAnAddress () {
            // ID1 KFA will always fail, so if the test setup is wrong, this test should fail
            AtomicInteger callCount = new AtomicInteger();

            addressProvider.fetchForPostcodes("ID1 KFA\n").subscribe(
                    address -> {
                        assertEquals(TEST_POSTCODE, address.postcode());
                        assertEquals(TEST_DISTRICT, address.town());
                        assertEquals(TEST_ADDRESS, address.address());
                        callCount.incrementAndGet();
                    },
                    error -> {fail("There should be no errors");}, // Do something testable…
                    callCount::incrementAndGet
            );

            // There should be an onNext and an onComplete
            assertEquals(2, callCount.get());
        }
    }

    @RunWith(SpringRunner.class)
    @SpringBootTest
    @ContextConfiguration(classes = {TestConfiguration.class})
    public static class WhenAPostcodeIsNotFound {

        @MockBean
        private WebClient webClient;

        @Autowired
        private AddressProvider addressProvider;

        /*
        When a no lookups are left, the error is PAYMENT_REQUIRED (buy more lookups)
         */
        @Before
        public void setup () {
            given (webClient.fetchUrl(anyString(), any()))
                    .willThrow(new HttpClientErrorException (HttpStatus.NOT_FOUND));
        }

        @Test
        public void thenThereShouldBeAHAndled404Eror () {
            // ID1 1QD will always succeed, so if the test setup is wrong, this test should fail
            addressProvider.fetchForPostcodes("ID1 1QD").subscribe(
                    address -> {fail("There should be no successful behaviour");},
                    error -> {assertTrue (true);}, // Do something testable…
                    () -> {fail("There should be no successful behaviour");}
            );
        }
    }

    @RunWith(SpringRunner.class)
    @SpringBootTest
    @ContextConfiguration(classes = {TestConfiguration.class})
    public static class WhenNoLookupsRemain {

        @MockBean
        private WebClient webClient;

        @Autowired
        private AddressProvider addressProvider;

        /*
        When a no lookups are left, the error is PAYMENT_REQUIRED (buy more lookups)
         */
        @Before
        public void setup () {
            given (webClient.fetchUrl(anyString(), any()))
                    .willThrow(new HttpClientErrorException (HttpStatus.PAYMENT_REQUIRED));
        }

        @Test
        public void thenThereShouldBeAPaymentRequiredError () {
            // ID1 1QD will always succeed, so if the test setup is wrong, this test should fail
            addressProvider.fetchForPostcodes("ID1 1QD").subscribe(
                    address -> {fail("There should be no successful behaviour");},
                    error -> {assertTrue (true);}, // Do something testable…
                    () -> {fail("There should be no successful behaviour");}
            );
        }
    }

    @RunWith(SpringRunner.class)
    @SpringBootTest
    @ContextConfiguration(classes = {TestConfiguration.class})
    public static class WhenALookupLimitIsReached {
        @MockBean
        private WebClient webClient;
        
        @Autowired
        private AddressProvider addressProvider;

        /*
        When a limit is reached, the error is PAYMENT_REQUIRED
         */
        @Before
        public void setup () {
            given (webClient.fetchUrl(anyString(), any()))
                    .willThrow(new HttpClientErrorException (HttpStatus.PAYMENT_REQUIRED));
        }
        
        @Test
        public void thenThereShouldBeAPaymentRequiredError () {
            // ID1 1QD will always succeed, so if the test setup is wrong, this test should fail
            addressProvider.fetchForPostcodes("ID1 1QD").subscribe(
                    address -> {fail("There should be no successful behaviour");},
                    error -> {assertTrue (true);}, // Do something testable…
                    () -> {fail("There should be no successful behaviour");}
            );
        }
    }
}
