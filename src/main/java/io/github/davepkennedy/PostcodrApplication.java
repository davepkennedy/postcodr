package io.github.davepkennedy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@SpringBootApplication
public class PostcodrApplication implements CommandLineRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(PostcodrApplication.class);

    @Autowired
    private AddressProvider addressProvider;

    public static void main(String[] args) {
        SpringApplication.run(PostcodrApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        addressProvider.fetchForPostcodes("W6 0LG", "SW1A 2AA", "BT48 6DQ").subscribe(
                address -> {
                    System.out.println(address.address());
                    System.out.println(address.town());
                    System.out.println(address.postcode());
                },
                error -> {
                    LOGGER.error(error.getMessage());
                },
                () -> System.out.println("Complete!")
        );
    }
}
