package io.github.davepkennedy;

import io.reactivex.Observable;

/**
 * A very simple interface for a client which fetches objects from a URL
 * <p>
 * A class implementing the interface could use Apache HttpComponents
 * and Jackson JSON library under the hood to quite easily implement this.
 * <p>
 * A test can provide a simple WebClient which provides canned responses for
 * a given URL.
 */
public interface WebClient {
    /**
     * Attempt to fetch some data from the provided URL and create an
     * Object of the given type from it.
     * <p>
     * Since we're returning an observable of T, the specific exceptions
     * do not need to be tied to the interface definition
     *
     * @param url   The url to fetch data from.
     * @param klass The class of object we want the data deserialized to
     * @param <T>
     * @return An observable value of T
     */
    <T> Observable<T> fetchUrl(String url, Class<T> klass);
}
