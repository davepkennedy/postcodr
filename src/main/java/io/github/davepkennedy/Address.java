package io.github.davepkennedy;

/**
 * A simple address interface which provides a postcode, address and a town.
 * <p>
 * The intent of this interface is to provide immutable objects.
 */
public interface Address {
    String postcode();

    String town();

    String address();
}
