package io.github.davepkennedy;

import io.reactivex.Observable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

/**
 * Concrete implementation of the WebClient using the Spring RestTemplate
 */
public class WebClientImpl implements WebClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebClientImpl.class);
    private RestTemplate restTemplate = new RestTemplate();

    @Override
    public <T> Observable<T> fetchUrl(String url, Class<T> klass) {
        LOGGER.info("Fetching addresses from {}", url);
        try {
            return Observable.just(restTemplate.getForObject(url, klass));
        } catch (HttpClientErrorException e) {
            return Observable.error(e);
        }
    }
}
