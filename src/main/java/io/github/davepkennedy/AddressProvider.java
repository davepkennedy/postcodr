package io.github.davepkennedy;

import io.reactivex.Observable;

/**
 * The basic interface for something which provides addresses for a given
 * postcode.
 */
public interface AddressProvider {
    /**
     * Obtain addresses from some source given one or more postcodes
     *
     * @param postcodes An array of postcodes
     * @return An observable of addresses. Will produce addresses until
     * either there are no more postcodes or there is an error.
     */
    Observable<Address> fetchForPostcodes(String... postcodes);
}
