package uk.co.idealpostcodes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Result {
    @JsonProperty ("line_1")
    private String line1;

    @JsonProperty ("line_2")
    private String line2;

    @JsonProperty ("thoroughfare")
    private String thoroughfare;

    @JsonProperty ("county")
    private String county;

    @JsonProperty ("country")
    private String country;

    @JsonProperty ("postcode")
    private String postcode;

    @JsonProperty ("organisation_name")
    private String organisation;

    @JsonProperty ("district")
    private String district;

    public String line1 () {
        return line1;
    }

    public String line2 () {
        return line2;
    }

    public String thoroughfare () {
        return thoroughfare;
    }

    public String county() {
        return county;
    }

    public String country() {
        return country;
    }

    public String postcode() {
        return postcode;
    }

    public String organisation() {
        return organisation;
    }

    public String district() {
        return district;
    }
}
