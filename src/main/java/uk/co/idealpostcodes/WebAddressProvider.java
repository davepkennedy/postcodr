package uk.co.idealpostcodes;

import io.github.davepkennedy.Address;
import io.github.davepkennedy.AddressProvider;
import io.github.davepkennedy.WebClient;
import io.reactivex.Observable;

import org.springframework.beans.factory.annotation.Autowired;

public class WebAddressProvider implements AddressProvider {

    /*
    Actual Url format is
    https://api.ideal-postcodes.co.uk/v1/postcodes/<postcode>?api_key=<apikey>
    */
    private static final String SERVICE_URI =
            "https://api.ideal-postcodes.co.uk/v1/postcodes/";
    private static final String KEY_PARAM = "?api_key=";

    @Autowired
    private WebClient client;

    /**
     * For humans, the postcode has a space separating the two parts.
     * For the service, the space needs to be removed
     * @param postcode The unformatted postcode
     * @return A String containing the postcode in a service consumable format
     */
    private static String formatPostcode (String postcode) {
        return postcode.replace(" ", "");
    }

    private static Address addressFromResult (Result result) {
        return new Address() {
            @Override
            public String postcode() {
                return result.postcode();
            }

            @Override
            public String town() {
                return result.district();
            }

            @Override
            public String address() {
                return String.join(",", result.line1(), result.line2());
            }
        };
    }

    private Observable<Address> fromResponse (Response response) {
        return Observable.fromIterable(response.results())
                .map(WebAddressProvider::addressFromResult);
    }

    private static String getApiKey () {
        return System.getProperty("apikey");
    }

    private static String createUrl (String postcode) {
        return SERVICE_URI + postcode + KEY_PARAM + getApiKey ();
    }

    private Observable<Response> fetchPostcode (String postcode) {
        return client.fetchUrl(createUrl(formatPostcode(postcode)), Response.class);
    }

    @Override
    public Observable<Address> fetchForPostcodes (String... postcodes) {
        return Observable.fromArray(postcodes)
                .flatMap(this::fetchPostcode)
                .flatMap(this::fromResponse);
    }
}
