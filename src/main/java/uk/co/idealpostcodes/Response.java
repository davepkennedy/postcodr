package uk.co.idealpostcodes;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Response {
    @JsonProperty ("result")
    private List<Result> results;

    public List<Result> results() {
        return results;
    }
}
